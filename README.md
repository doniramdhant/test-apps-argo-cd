#### Commands

```bash
# cara install argocd
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

# cara akses ke argo menggunakan port-foward
kubectl get svc -n argocd
kubectl port-forward svc/argocd-server 8080:443 -n argocd

# ambil admin password mengikuti dokumentasi di argocd
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode && echo

# boleh dihapus pake init

```